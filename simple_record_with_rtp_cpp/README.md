# Overview

Simple application to record a RTP stream and put it into a wav file. This experiment
will be the base for the record functionality on the HMP Sixbell

# Compilation
## Linux, Ubuntu 18.04
```
cmake .
make clean
make
```
This will create ```rec_server``` binary

# Execution
```
./rec_server <wav file>
```

 * **wav file:** file where ```rec_server``` will store audio streaming in 
 ```wav format```
 
Example:
```
./rec_server temp1.wav
```


