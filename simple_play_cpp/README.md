# Overview
Simple application got from 
[gstreamer basic docs](https://gstreamer.freedesktop.org/documentation/application-development/basics/helloworld.html), 
the idea is to play with it!. This application in particular playback an ```ogg``` file.

# Compilation
## MacOSX
```
c++ -I /Library/Frameworks/GStreamer.framework/Headers -framework GStreamer main.cpp
```
If I use C++ bindings
```
c++ -std=c++17 -I /Library/Frameworks/GStreamer.framework/Headers -I/usr/local/include/gstreamermm-1.0 -I/usr/local/lib/gstreamermm-1.0/include/ -I/usr/local/Cellar/glibmm/2.58.0/lib/glibmm-2.4/include -I/usr/local/Cellar/glibmm/2.58.0/lib/glibmm-2.4/include -I/usr/local/Cellar/glibmm/2.58.0/include/glibmm-2.4 -I/usr/local/Cellar/libsigc++/2.10.1/include/sigc++-2.0 -I/usr/local/Cellar/libsigc++/2.10.1/lib/sigc++-2.0/include -I/usr/local/Cellar/glibmm/2.58.0/include/giomm-2.4 \-I/usr/local/lib/giomm-2.4/include -framework GStreamer main.cpp -lgstreamermm-1.0
```

# Execution
```
./a.out <Ogg/Vorbis filename>
```
 
Example:
```
./a.out file_example_OOG_1MG.ogg
```

You should see something like this, if the execution is successful:
```
Now playing: file_example_OOG_1MG.ogg
Running...
Dynamic pad created, linking demuxer -> decoder
End of stream
Returned, stopping playback
Deleting pipeline
```
