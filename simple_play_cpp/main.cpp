#include <glib.h>
#include <gst/gst.h>

#include <iostream>
#include <string>

using namespace std;

struct Gstreamer {
  Gstreamer(int argc, char *argv[]) { gst_init(&argc, &argv); }
};

struct Element {
  Element(string type, string label) {
    h = gst_element_factory_make(type.c_str(), label.c_str());
  }

  void set_property(string source, string file) {
    g_object_set(G_OBJECT(h), source.c_str(), file.c_str(), NULL);
  }

  void link(Element &e) { gst_element_link(h, e.h); }

  GstElement *h;
};

struct Pipeline {
  Pipeline() { p = gst_pipeline_new("audio-player"); }
  ~Pipeline() { gst_object_unref(GST_OBJECT(p)); }

  void set_state(GstState state) { gst_element_set_state(p, state); }
  void add(Element e) { gst_bin_add(GST_BIN(p), e.h); }

  GstElement *p;
};

struct Loop {
  Loop() { l = g_main_loop_new(NULL, FALSE); }
  ~Loop() { g_main_loop_unref(l); }

  void run() { g_main_loop_run(l); }

  GMainLoop *l;
};

struct Bus {
  Bus(Pipeline &p) { b = gst_pipeline_get_bus(GST_PIPELINE(p.p)); }
  ~Bus() { gst_object_unref(b); }

  GstBus *b;
};

static gboolean bus_call(GstBus *bus, GstMessage *msg, gpointer data) {
  GMainLoop *loop = (GMainLoop *)data;
  switch (GST_MESSAGE_TYPE(msg)) {
    case GST_MESSAGE_EOS: {
      g_print("End of stream\n");
      g_main_loop_quit(loop);
      break;
    }

    case GST_MESSAGE_ERROR: {
      gchar *debug;
      GError *error;

      gst_message_parse_error(msg, &error, &debug);
      g_free(debug);

      g_printerr("Error: %s\n", error->message);
      g_free(error);

      g_main_loop_quit(loop);
      break;
    }

    default: { break; }
  }
  return TRUE;
}

static void on_pad_added(GstElement *element, GstPad *pad, gpointer data) {
  GstElement *decoder = (GstElement *)data;

  /*We can now link this pad with the vorbis-decoder sinkpad*/
  g_print("Dynamic pad created, linking demuxer -> decoder\n");

  GstPad *sinkpad = gst_element_get_static_pad(decoder, "sink");
  gst_pad_link(pad, sinkpad);
  gst_object_unref(sinkpad);
}

int main(int argc, char *argv[]) {
  /*Checking stdin arguments*/
  if (argc != 2) {
    g_printerr("Usage: %s <Ogg/Vorbis filename>\n", argv[0]);
    return -1;
  }

  Gstreamer gstreamer(argc, argv);

  Loop loop;
  Pipeline pipeline;
  Element source("filesrc", "file-source"), demuxer("oggdemux", "ogg-demuxer"),
      decoder("vorbisdec", "vorbis-decoder"), conv("audioconvert", "converter"),
      sink("autoaudiosink", "audio-output");

  if (!pipeline.p || !source.h || !demuxer.h || !decoder.h || !conv.h ||
      !sink.h) {
    g_printerr("One element could not be created. Exiting\n");
  }

  source.set_property("location", argv[1]);

  /*We add a message handler for the pipeline*/
  Bus bus(pipeline);
  guint bus_watch_id = gst_bus_add_watch(bus.b, bus_call, loop.l);

  pipeline.add(source);
  pipeline.add(demuxer);
  pipeline.add(decoder);
  pipeline.add(conv);
  pipeline.add(sink);

  source.link(demuxer);
  decoder.link(conv);
  conv.link(sink);

  g_signal_connect(demuxer.h, "pad-added", G_CALLBACK(on_pad_added), decoder.h);

  cout << "Now playing: " << argv[1] << endl;
  pipeline.set_state(GST_STATE_PLAYING);

  cout << "Running..." << endl;
  loop.run();

  cout << "Returned, stopping playback" << endl;
  pipeline.set_state(GST_STATE_NULL);

  cout << "Deleting pipeline" << endl;
  g_source_remove(bus_watch_id);

  return 0;
}