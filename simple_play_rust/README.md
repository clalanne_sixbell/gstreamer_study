# Overview
Simple application got from 
[gstreamer basic docs](https://gstreamer.freedesktop.org/documentation/application-development/basics/helloworld.html), 
the idea is to play with it!. This application in particular playback an ```ogg``` file.
This one use ```rust``` as programming language instead of C or C++.

# Compilation
First check 
[gstreamer's rust binding installation](https://crates.io/crates/gstreamer)

## MacOSX
Remember to set:
```
export PKG_CONFIG_PATH="/Library/Frameworks/GStreamer.framework/Versions/Current/lib/pkgconfig${PKG_CONFIG_PATH:+:$PKG_CONFIG_PATH}"
```

And from the root of the project:
```
cargo build
```

# Execution
```
cargo run <Ogg/Vorbis filename>
```
 
Example:
```
cargo run file_example_OOG_1MG.ogg
```

You should see something like this, if the execution is successful:
```
Now playing: file_example_OOG_1MG.ogg
Running...
Dynamic pad created, linking demuxer -> decoder
End of stream
Returned, stopping playback
Deleting pipeline
```
