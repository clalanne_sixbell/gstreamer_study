extern crate gstreamer as gst;
use gst::prelude::*;

extern crate failure;
use failure::Error;

extern crate glib;

use std::env;

#[macro_use]
extern crate failure_derive;

#[derive(Debug, Fail)]
#[fail(display = "Missing element {}", _0)]
struct MissingElement(&'static str);

fn play(file: &str) -> Result<(), Error> {
    gst::init()?;

    let main_loop = glib::MainLoop::new(None, false);
    /*Creation of gstreamer elements*/
    let pipeline = gst::Pipeline::new("audio-player");
    let source =
        gst::ElementFactory::make("filesrc", "file-source").ok_or(MissingElement("filesrc"))?;
    let demuxer =
        gst::ElementFactory::make("oggdemux", "ogg-demuxer").ok_or(MissingElement("oggdemux"))?;
    let decoder = gst::ElementFactory::make("vorbisdec", "vorbis-decoder")
        .ok_or(MissingElement("vorbisdec"))?;
    let conv = gst::ElementFactory::make("audioconvert", "converter")
        .ok_or(MissingElement("audioconvert"))?;
    let sink = gst::ElementFactory::make("autoaudiosink", "audio-output")
        .ok_or(MissingElement("audiosink"))?;

    /*Setup de pipeline*/
    /*Set the input file to the source element*/
    println!("file to be used: {}", file);

    source.set_property("location", &file)?;

    let bus = pipeline
        .get_bus()
        .expect("Pipeline without bus. Shouldn't happen!");

    pipeline.add_many(&[&source, &demuxer, &decoder, &conv, &sink])?;

    source
        .link(&demuxer)
        .expect("Elements could not be linked.");
    gst::Element::link_many(&[&decoder, &conv, &sink])?;

    let pipeline_weak = pipeline.downgrade();
    let decoder_weak = decoder.downgrade();
    demuxer.connect_pad_added(move |_, demuxer_pad| {
        let pipeline = match pipeline_weak.upgrade() {
            Some(pipeline) => pipeline,
            None => return,
        };

        let decoder = match decoder_weak.upgrade() {
            Some(decoder) => decoder,
            None => return,
        };

        println!(
            "Received new pad {} from {}",
            demuxer_pad.get_name(),
            pipeline.get_name()
        );

        let sink_pad = decoder
            .get_static_pad("sink")
            .expect("Failed to get static sink pad from decoder");

        if sink_pad.is_linked() {
            println!("We are ready linked. Ignoring!");
            return;
        }

        demuxer_pad.link(&sink_pad);
    });

    println!("Now playing: {}", &file);
    let ret = pipeline.set_state(gst::State::Playing);
    assert_ne!(ret, gst::StateChangeReturn::Failure);

    let main_loop_clone = main_loop.clone();
    bus.add_watch(move |_, msg| {
        use gst::MessageView;

        let main_loop = &main_loop_clone;
        match msg.view() {
            MessageView::Eos(..) => {
                println!("End of stream");
                main_loop.quit();
            }
            MessageView::Error(err) => {
                println!(
                    "Error from {:?}: {} ({:?})",
                    err.get_src().map(|s| s.get_path_string()),
                    err.get_error(),
                    err.get_debug()
                );
                main_loop.quit();
            }
            _ => (),
        };
        glib::Continue(true)
    });

    println!("Running...");
    main_loop.run();
    let ret = pipeline.set_state(gst::State::Null);
    assert_ne!(
        ret,
        gst::StateChangeReturn::Failure,
        "Unable to set the pipeline to the Null state."
    );

    Ok(())
}

fn main() {
    let args: Vec<_> = env::args().collect();
    let file: &str = if args.len() == 2 {
        args[1].as_ref()
    } else {
        println!("Usage: {} <Ogg/Vorbis filename>", args[0]);
        std::process::exit(-1)
    };

    match play(&file) {
        Ok(r) => r,
        Err(e) => eprintln!("Error! {}", e),
    }
}
