# Overview
Simple streaming of audio file via ```RTP```, using ```G711``` ```u law```. This 
application uses ```Rust``` as language.

# Compilation
First check 
[gstreamer's rust binding installation](https://crates.io/crates/gstreamer)

## MacOSX
Remember to set:
```
export PKG_CONFIG_PATH="/Library/Frameworks/GStreamer.framework/Versions/Current/lib/pkgconfig${PKG_CONFIG_PATH:+:$PKG_CONFIG_PATH}"
```

And from the root of the project:
```
cargo build
```

# Execution

## Sender side
The sender side is this application
```
cargo run <Ogg/Vorbis filename>
```
 
Example:
```
cargo run file_example_OOG_1MG.ogg
```

You should see something like this, if the execution is successful:
```
    Finished dev [unoptimized + debuginfo] target(s) in 0.06s
     Running `target/debug/simple_play_rust file_example_OOG_1MG.ogg`
Hello, world!
Now playing: file_example_OOG_1MG.ogg
Running...
Received new pad src_0000616e from audio-player

End of stream
```

## Receiving side

We can use ```gst-launch-1.0``` to start a media server very easily
```
gst-launch-1.0 udpsrc port=5555 caps="application/x-rtp" ! queue ! rtppcmudepay ! mulawdec ! audioconvert ! autoaudiosink
```


