#include <gst/gst.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
  /*showing gstreamer version*/
  const gchar *nano_str;
  guint major, minor, micro, nano;

  gst_init(&argc, &argv);
  gst_version(&major, &minor, &micro, &nano);

  if (nano == 1) {
    nano_str = "(CVS)";
  } else if (nano == 2) {
    nano_str = "(Prerelease)";
  } else {
    nano_str = "";
  }

  printf("This program is linked against Gstreamer %d.%d.%d.%s\n", major, minor,
         micro, nano_str);

  /*Element creation*/
  GstElement *element;
  element = gst_element_factory_make("fakesrc", "source");
  if (!element) {
    g_print("Failed to create element of type 'fakesrc'\n");
    return -1;
  }

  /*get name property*/
  gchar *name;
  g_object_get(G_OBJECT(element), "name", &name, NULL);

  g_print("The name of the element is '%s'.\n", name);

  gst_object_unref(GST_OBJECT(element));

  /*Playing with factories*/
  GstElementFactory *factory;
  factory = gst_element_factory_find("fakesrc");
  if (!factory) {
    g_print("You do NOT have a 'fakesrc' element installed!\n");
  }

  /*display factory information*/
  g_print(
      "The '%s' element is a member of the category %s.\n"
      "Description: %s\n",
      gst_plugin_feature_get_name(GST_PLUGIN_FEATURE(factory)),
      gst_element_factory_get_metadata(factory, GST_ELEMENT_METADATA_KLASS),
      gst_element_factory_get_metadata(factory,
                                       GST_ELEMENT_METADATA_DESCRIPTION));

  gst_object_unref(GST_OBJECT(factory));

  /*creation of a simple and basic pipeline*/
  /*TODO: not releasing anything in this piece of code
  becareful with that!!!!*/
  GstElement *pipeline;
  GstElement *source, *filter, *sink;

  pipeline = gst_pipeline_new("my-pipeline");

  source = gst_element_factory_make("fakesrc", "source");
  filter = gst_element_factory_make("identity", "filter");
  sink = gst_element_factory_make("fakesink", "sink");

  gst_bin_add_many(GST_BIN(pipeline), source, filter, sink, NULL);
  if (!gst_element_link_many(source, filter, sink, NULL)) {
    g_warning("Failed to link elements!");
  }

  /*destroyin the pipeline created*/
  // TODO: not sure about this snippet
  // gst_element_set_state(pipeline, GST_STATE_NULL);
  // g_object_unref(pipeline);

  /*using bins*/
  GstElement *bin;
  bin = gst_bin_new("my_bin");

  GstElement *source1, *sink1;
  source1 = gst_element_factory_make("fakesrc", "source1");
  sink1 = gst_element_factory_make("fakesink", "sink1");

  gst_bin_add_many(GST_BIN(bin), source1, sink1, NULL);

  GstElement *pipeline1;
  pipeline1 = gst_pipeline_new("my-pipeline");
  gst_bin_add(GST_BIN(pipeline1), bin);

  if (!gst_element_link(source1, sink1)) {
    g_warning("Failed to link elements in bin exercise!");
  }

  return 0;
}
