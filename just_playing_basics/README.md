# Overview
Just Playing with code snippets from 
[gstreamer documentation basics](https://gstreamer.freedesktop.org/documentation/application-development/basics/)
and trying to understand gstreamer's basic concepts. I mean really basic stuff, just
warming up.



# Compilation 
## MacOSX ##
```
c++ -I /Library/Frameworks/GStreamer.framework/Headers -framework GStreamer main.cpp
```

# Execution
```
./a.out
```

You should see something like

```
This program is linked against Gstreamer 1.14.4.
The name of the element is 'source'.
The 'fakesrc' element is a member of the category Source.
Description: Push empty (no data) buffers around
```

# Notes
 * No ```Pad``` regarding code here!
